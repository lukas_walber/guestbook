<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Service;

class HttpService
{
    //TODO tests
    public function redirectTo(string $path): void
    {
        header('Location: /' . $path);
        die();
    }

    public function get(string $key) : ?string
    {
        if (array_key_exists($key, $_GET)) {
            return $_GET[$key];
        }

        return null;
    }

    public function post(string $key) : ?string
    {
        if (array_key_exists($key, $_POST)) {
            return $_POST[$key];
        }

        return null;
    }

    public function buildUrlWithGetValues(
        string $redirectTo,
        ?string $username = null,
        ?string $email = null,
        ?string $firstName = null,
        ?string $lastName = null
    ): string {
        if ($username === null && $email === null && $firstName === null && $lastName === null) {
            return $redirectTo;
        }

        $redirectTo .= '?';
        if ($username !== null) {
            $redirectTo .= 'username='.$username.'&';
        }
        if ($email !== null) {
            $redirectTo .= 'email='.$email.'&';
        }
        if ($firstName !== null) {
            $redirectTo .= 'first_name='.$firstName.'&';
        }
        if ($lastName !== null) {
            $redirectTo .= 'last_name='.$lastName;
        }

        return $redirectTo;
    }
}