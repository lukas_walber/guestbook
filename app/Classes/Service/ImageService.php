<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Service;

use Bulletproof\Image;
use RealDigital\GuestBook\Exception\UploadException;

class ImageService
{
    //TODO tests
    const DIR = 'resources/public/image';
    /**
     * @throws UploadException
     */
    public function uploadPicture(): string
    {
        $image = new Image($_FILES);
        $image->setLocation(self::DIR);

        if ($image['photo'] === false) {
            throw new UploadException('photo not found');
        }

        $upload = $image->upload();
        if ($upload === false) {
            throw new UploadException($image->getError());
        }

        return $upload->getFullPath();
    }
}