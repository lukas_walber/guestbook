<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Service;

use RealDigital\GuestBook\Message\MessageInterface;

class SessionService
{
    //TODO tests
    public function checkLogin(): bool
    {
        if(!isset($_SESSION['login'])) {
            return false;
        }

        return $_SESSION['login'];
    }

    public function setLogin(bool $login = true): void
    {
        $_SESSION['login'] = $login;
    }

    public function setUserId(int $id): void
    {
        $_SESSION['user']['id'] = $id;
    }

    public function getUserId(): ?int
    {
        if(!isset($_SESSION['user']['id'])) {
            return null;
        }

        return $_SESSION['user']['id'];
    }

    public function resetUser(): void
    {
        $_SESSION['user'] = null;
    }

    public function setMessage(MessageInterface $message): void
    {
        $_SESSION['message'] = $message->toArray();
    }

    public function resetMessage(): void
    {
        $_SESSION['message'] = null;
    }

    public function getMessage(): ?array
    {
        if ($_SESSION === null) {
            return null;
        }

        if (!array_key_exists('message', $_SESSION)) {
            return null;
        }

        return $_SESSION['message'];
    }

    public function addFailedLogin(): void
    {
        if (!isset($_SESSION['failedLogins'])){
            $_SESSION['failedLogins'] = 1;
            return;
        }

        $_SESSION['failedLogins']++;
        return;
    }

    public function checkFailedLogins(): int
    {
        if (isset($_SESSION['failedLogins'])) {
            return $_SESSION['failedLogins'];
        }

        return $_SESSION['failedLogins'] = 0;
    }

    public function resetSessionTimer(): void
    {
        $_SESSION['lastActivity'] = $_SERVER['REQUEST_TIME'];
        return;
    }

    public function timeoutCheck(int $timeoutDuration = 300): bool
    {
        return ($_SERVER['REQUEST_TIME'] - $_SESSION['lastActivity']) > $timeoutDuration;
    }

    public function timeout(): void
    {
        session_unset();
        session_destroy();
        self::startSession();
    }

    public function startSession(): void
    {
        session_start();
        self::resetSessionTimer();
    }

    public function howLongUntilSessionReset(): int
    {
        return $resetTime = $_SERVER['REQUEST_TIME'] - $_SESSION['lastActivity'];
    }
}