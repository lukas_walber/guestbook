<?php
declare(strict_types=1);


namespace RealDigital\GuestBook\Mailer;

class StandardMailer
{
    //TODO tests
    public static function sendHtmlMail(string $emailTo, string $emailFrom, string $subject, string $htmlBodyMail): bool
    {
        $header = 'MIME-Version: 1.0\r\n';
        $header .= 'Content-type: text/html; charset=utf-8\r\n';
        $header .= 'From: '. $emailFrom . '\r\n';
        $header .= 'Reply-To: '. $emailFrom .'\r\n';
        $header .= 'X-Mailer: PHP ' . phpversion();

        return mail($emailTo, $subject, $htmlBodyMail, $header);
    }
}