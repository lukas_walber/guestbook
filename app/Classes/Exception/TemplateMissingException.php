<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Exception;

class TemplateMissingException extends \Exception
{

}