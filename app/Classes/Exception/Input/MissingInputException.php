<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Exeption\Input;

class MissingInputException extends InputException
{

}