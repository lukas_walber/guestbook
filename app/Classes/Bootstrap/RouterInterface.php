<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Bootstrap;

interface RouterInterface
{
    public function add(string $url, callable $method): void;

    public function run(): void;
}