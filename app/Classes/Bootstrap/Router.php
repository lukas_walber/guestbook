<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Bootstrap;

class Router implements RouterInterface
{
    //TODO tests
    private $routes = [];

    public function add(string $url, callable $method): void
    {
        $this->routes[$url] = $method;
    }

    public function run(): void
    {
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        if (!array_key_exists($path, $this->routes)) {
            http_response_code(404);
            return;
        }
        call_user_func($this->routes[$path]);
    }


}