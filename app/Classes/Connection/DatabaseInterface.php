<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Connection;

interface DatabaseInterface
{
    public function escapeString(string $string): string;

    public function selectQuery(
        string $tableName,
        string $column = '*',
        string $where = '',
        string $order = '',
        int $limit = 0,
        int $offset = 0
    ): ?array;

    public function count(string $tableName, string $where = ''): int;

    public function insertQuery(string $tableName, array $insertParams): ?int;

    public function updateQuery(string $tableName, array $updateParams, string $where = ''): bool;

    public function deleteQuery(string $tableName, string $where): bool;
}