<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Connection;

class Database implements DatabaseInterface
{
    /** @var null|\mysqli $mysqli */
    private $mysqli = null;
    /** @var string $user */
    private $user = 'root';
    /** @var string $password */
    private $password = 'root';
    /** @var string $host */
    private $host = 'db';
    /** @var int $port */
    private $port = 3306;
    /** @var string $database */
    private $database = 'guest_book';

    public function __construct(
        string $user = null,
        string $password = null,
        string $host = null,
        int $port = null,
        string $database = null
    )
    {
        if (!is_null($user)) {
            $this->user = $user;
        }
        if (!is_null($password)) {
            $this->password = $password;
        }
        if (!is_null($host)) {
            $this->host = $host;
        }
        if (!is_null($port)) {
            $this->port = $port;
        }
        if (!is_null($database)) {
            $this->database = $database;
        }

        $this->connect();
    }

    private function connect(): void
    {
        $this->mysqli = new \mysqli($this->host, $this->user, $this->password, $this->database, $this->port);
        if ($this->mysqli->connect_error) {
            die('Connection Error (' . $this->mysqli->connect_error . ') ' . $this->mysqli->connect_error);
        }
    }

    public function __destruct()
    {
        $this->disconnect();
    }

    private function disconnect(): void
    {
        if (is_null($this->mysqli)) {
            return;
        }

        $this->mysqli->close();
    }

    public function escapeString(string $string): string
    {
        return $this->mysqli->escape_string($string);
    }

    public function selectQuery(
        string $tableName,
        string $column = '*',
        string $where = '',
        string $order = '',
        int $limit = 0,
        int $offset = 0
    ): ?array {
        $query = sprintf('SELECT * FROM %s', $tableName);

        if ($column !== '') {
        $query = sprintf('SELECT %s FROM %s', $column, $tableName);
        }
        if ($where !== '') {
            $query .= ' WHERE '. $where;
        }
        if ($order !== '') {
            $query .= ' ORDER '. $order;
        }
        if ($limit !== 0) {
            $query .= ' LIMIT '. $limit;
        }
        if ($offset !== 0) {
            $query .= ' OFFSET '. $offset;
        }
        $result = $this->mysqli->query($query);
        if (!$result) {
            return null;
        }
        if ($result->num_rows === 0) {
            $result->free();
            return null;
        }

        if ($result->num_rows === 1) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $result->free();
            return $row;
        }

        $rows = [];
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        $result->free();

        return $rows;
    }

    public function insertQuery(string $tableName, array $insertParams): ?int
    {
        $columns = [];
        $values = [];

        foreach ($insertParams as $column => $value) {
            $columns[] = $column;
            $values[] = $value;
        }

        $query = sprintf(
            'INSERT INTO %s (%s) VALUE ("%s")',
            $tableName,
            implode($columns, ', '),
            implode($values, '", "')
        );
        $result = $this->mysqli->query($query);
        if (!$result) {
            return null;
        }

        return $this->mysqli->insert_id;
    }

    public function updateQuery(string $tableName, array $updateParams, string $where = ''): bool
    {
        $update = [];
        foreach ($updateParams as $column => $value) {
            $update[] = $column . ' = ' . "'" . $value . "'";
        }

        $updateStatement = implode($update, ', ');

        $query = sprintf('UPDATE %s SET %s', $tableName, $updateStatement);

        if ($where !== '') {
            $query = sprintf('UPDATE `%s` SET %s  WHERE %s', $tableName, $updateStatement, $where);
        }

        $result = $this->mysqli->query($query);

        if (!$result || $this->mysqli->affected_rows === 0) {
            return false;
        }

        return true;
    }

    public function deleteQuery(string $tableName, string $where): bool
    {
        $query = sprintf('DELETE FROM %s WHERE %s', $tableName, $where);

        $result = $this->mysqli->query($query);

        if (!$result) {
            return false;
        }

        return true;
    }

    public function count(string $tableName, string $where = ''): int
    {
        if ($where !== '') {
            $where = sprintf(' WHERE %s', $where);
        }
        $query = sprintf('SELECT COUNT(id) as \'count\' FROM %s%s', $tableName, $where);

        $result = $this->mysqli->query($query);

        if (!$result) {
            return 0;
        }

        $row = $result->fetch_array(MYSQLI_ASSOC);

        return (int) $row['count'];
    }
}