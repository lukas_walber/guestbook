<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\TemplateEngine;

interface ParserInterface
{
    public function render(array $vars, string $content): string;

    public function getVarFromContent(string $content): array;
}