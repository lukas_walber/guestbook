<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\TemplateEngine;

class MarkerParser implements ParserInterface
{
    const MARKER_PATTERN = '###';

    public function render(array $vars, string $content): string
    {
        foreach ($vars as $key => $var) {
            $content = str_replace(
                self::MARKER_PATTERN . strtoupper($key) . self::MARKER_PATTERN,
                $var,
                $content
            );
        }
        return $content;
    }

    public function getVarFromContent(string $content): array
    {
        $pattern = '/'. self::MARKER_PATTERN . '([A-Z_]+)?' . self::MARKER_PATTERN .'/';
        preg_match_all($pattern, $content , $matches);
        $uniqueMatches = array_unique($matches[1]);

        if ($uniqueMatches === []) {
            return [];
        }

        $markers = [];
        foreach ($uniqueMatches as $match) {
            $markers[] = strtolower($match);
        }
        return $markers;
    }
}