<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\TemplateEngine;

use RealDigital\GuestBook\Exception\TemplateMissingException;

class HtmlTemplate implements TemplateInterface
{
    /** @var string */
    private $pathToTemplates = '/resources/private/template/';

    /** @var string */
    private $pathToTemplate;

    /** @var string */
    private $content;

    public function __construct(?string $pathToTemplates = null)
    {
        if ($pathToTemplates !== null) {
            $this->pathToTemplates = $pathToTemplates;
        }
    }

    public function setTemplate(string $templateName): void
    {
        $this->pathToTemplate = $_SERVER['DOCUMENT_ROOT'] . $this->pathToTemplates . $templateName . '.html';
        if (!$this->templateExists()) {
            throw new TemplateMissingException($templateName . ' Template not found!');
        }

        $this->content = file_get_contents($this->pathToTemplate);
    }

    public function getContent(): string
    {
        return $this->content;
    }

    private function templateExists(): bool
    {
        return file_exists($this->pathToTemplate);
    }
}