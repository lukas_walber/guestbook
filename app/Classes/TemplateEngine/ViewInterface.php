<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\TemplateEngine;

use RealDigital\GuestBook\Exception\TemplateMissingException;

interface ViewInterface
{
    public function assignSingle(string $key, string $value): void;

    public function assignMulti(array $arguments): void;

    public function render(bool $resetViewAfter = true): string;

    /**
     * @throws TemplateMissingException
     */
    public function setTemplate(string $templateName): void;
}