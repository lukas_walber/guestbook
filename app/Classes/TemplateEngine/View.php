<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\TemplateEngine;

class View implements ViewInterface
{
    /** @var string[] */
    private $variables = [];

    /** @var TemplateInterface */
    private $template;

    /** @var ParserInterface */
    private $parser;

    public function __construct(TemplateInterface $template, ParserInterface $parser)
    {
        $this->template = $template;
        $this->parser = $parser;
    }

    public function assignSingle(string $key, string $value): void
    {
        $this->variables[$key] = $value;
    }

    public function assignMulti(array $arguments): void
    {
        foreach ($arguments as $key => $value) {
            $this->variables[$key] = $value;
        }
    }

    public function setTemplate(string $templateName): void
    {
        $this->template->setTemplate($templateName);
    }

    public function render(bool $replaceNotAssignedVariables = true): string
    {
        if ($replaceNotAssignedVariables) {
            $this->ensureThatAllVariablesFromTheTemplateAreAssigned();
        }
        $html = $this->parser->render($this->variables, $this->template->getContent());
        return $html;
    }

    private function ensureThatAllVariablesFromTheTemplateAreAssigned(): void
    {
        $variables = $this->parser->getVarFromContent($this->template->getContent());
        foreach ($variables as $variable) {
            if (!array_key_exists($variable, $this->variables)) {
                $this->assignSingle($variable, '');
            }
        }
    }
}