<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\TemplateEngine;

use RealDigital\GuestBook\Exception\TemplateMissingException;

interface TemplateInterface
{
    /**
     * @throws TemplateMissingException
     */
    public function setTemplate(string $templateName): void;

    public function getContent(): string;
}