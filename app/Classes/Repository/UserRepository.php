<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Repository;

use RealDigital\GuestBook\Connection\DatabaseInterface;
use RealDigital\GuestBook\Entity\UserEntity;
use RealDigital\GuestBook\Service\SessionService;

class UserRepository
{
    const TABLE_NAME = 'user';
    const COLUMN_ID = 'id';
    const COLUMN_USERNAME = 'username';
    const COLUMN_PASSWORD = 'password';
    const COLUMN_EMAIL = 'email';
    const COLUMN_FIRST_NAME = 'first_name';
    const COLUMN_LAST_NAME = 'last_name';
    const COLUMN_VERIFY_HASH = 'verify_hash';
    const COLUMN_VERIFIED = 'verified';
    const COLUMN_PHOTO = 'photo';
    const COLUMN_ROLE = 'role';

    /** @var DatabaseInterface $database */
    private $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function getUserById(int $id): ?UserEntity
    {
        $where = sprintf('%s = %s;',self::COLUMN_ID, $id);
        $columns = '*';
        $result = $this->database->selectQuery(self::TABLE_NAME, $columns, $where);

        if ($result === null) {
            return null;
        }

        return $this->convertToEntity($result);
    }

    public function getUserByUsername(string $username): ?UserEntity
    {
        $username = $this->database->escapeString($username);
        $column = '*';

        $where = sprintf("%s = '%s'",self::COLUMN_USERNAME, $username);
        $result = $this->database->selectQuery(self::TABLE_NAME, $column, $where);

        if ($result === null) {
            return null;
        }

        return $this->convertToEntity($result);
    }

    public function getUserByEmail(string $email): ?UserEntity
    {
        $email = $this->database->escapeString($email);

        $where = sprintf("%s = '%s'",self::COLUMN_EMAIL, $email);
        $result = $this->database->selectQuery(self::TABLE_NAME, '', $where);

        if ($result === null) {
            return null;
        }

        return $this->convertToEntity($result);
    }

    public function save(UserEntity $user): ?UserEntity
    {
        $params = [];
        if ($user->getUsername() !== null) {
            $username = $this->database->escapeString($user->getUsername());
            $params[self::COLUMN_USERNAME] = $username;
        }

        if ($user->getPassword() !== null) {
            $password = $this->database->escapeString($user->getPassword());
            $params[self::COLUMN_PASSWORD] = $password;
        }

        if ($user->getFirstName() !== null) {
            $firstName = $this->database->escapeString($user->getFirstName());
            $params[self::COLUMN_FIRST_NAME] = $firstName;
        }

        if ($user->getLastName() !== null) {
            $lastName = $this->database->escapeString($user->getLastName());
            $params[self::COLUMN_LAST_NAME] = $lastName;
        }

        if ($user->getEmail() !== null) {
            $email = $this->database->escapeString($user->getEmail());
            $params[self::COLUMN_EMAIL] = $email;
        }

        if ($user->getPhoto() !== null) {
            $photo = $this->database->escapeString($user->getPhoto());
            $params[self::COLUMN_PHOTO] = $photo;
        }

        $params[self::COLUMN_VERIFIED] = (int) $user->isVerified();

        if ($user->getId() === null) {
            return $this->createUser($user, $params);
        }

        return $this->updateUser($user, $params);
    }

    public function getAllUsers(): ?array
    {
        $users = $this->database->selectQuery(self::TABLE_NAME, '*');

        if ($users === null) {
            return null;
        }

        return $this->convertToEntityList($users);
    }

    public function deleteUser(string $userId): void
    {
        $where = sprintf('`%s`=%s',self::COLUMN_ID, $userId);
        $this->database->deleteQuery(self::TABLE_NAME, $where);
    }

    private function createUser(UserEntity $user, array $params): ?UserEntity
    {
        $hash = uniqid();
        $params[self::COLUMN_VERIFIED] = 0;
        $params[self::COLUMN_VERIFY_HASH] = $hash;
        $user->setVerifyHash($hash);

        $userId = $this->database->insertQuery(self::TABLE_NAME, $params);

        if ($userId === null) {
            return null;
        }
        $user->setId($userId);
        return $user;
    }

    private function updateUser(UserEntity $user, array $params): ?UserEntity
    {
        $where = sprintf("%s = %s", self::COLUMN_ID, $user->getId());
        $update = $this->database->updateQuery(self::TABLE_NAME, $params, $where);

        if ($update === false) {
            return null;
        }
        return $user;
    }

    private function convertToEntityList(array $results): array
    {
        $users = [];
        if (!isset($results[0])) {
            $users[] = $this->convertToEntity($results);
            return $users;
        }

        foreach ($results as $result) {
            $users[] = $this->convertToEntity($result);
        }

        return $users;
    }

    private function convertToEntity(array $result): ?UserEntity
    {
        $user = new UserEntity();

        if ($result[self::COLUMN_ID] !== null) {
            $user->setId((int)$result[self::COLUMN_ID]);
        }
        if ($result[self::COLUMN_USERNAME] !== null) {
            $user->setUsername($result[self::COLUMN_USERNAME]);
        }
        if ($result[self::COLUMN_FIRST_NAME] !== null) {
            $user->setFirstName($result[self::COLUMN_FIRST_NAME]);
        }
        if ($result[self::COLUMN_LAST_NAME] !== null) {
            $user->setLastName($result[self::COLUMN_LAST_NAME]);
        }
        if ($result[self::COLUMN_EMAIL] !== null) {
            $user->setEmail($result[self::COLUMN_EMAIL]);
        }
        if ($result[self::COLUMN_PASSWORD] !== null) {
            $user->setPassword($result[self::COLUMN_PASSWORD]);
        }
        if ($result[self::COLUMN_VERIFY_HASH] !== null) {
            $user->setVerifyHash($result[self::COLUMN_VERIFY_HASH]);
        }
        if ($result[self::COLUMN_PHOTO] !== null) {
            $user->setPhoto($result[self::COLUMN_PHOTO]);
        }
        if ($result[self::COLUMN_ROLE] !== null) {
            $user->setRole((int) $result[self::COLUMN_ROLE]);
        }
        if ($result[self::COLUMN_VERIFIED] !== null) {
            $user->setVerified((bool)$result[self::COLUMN_VERIFIED]);
        }

        return $user;
    }

    public function encryptPassword(string $password): string
    {
        return md5($password);
    }

}
