<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Repository;

use RealDigital\GuestBook\Connection\DatabaseInterface;
use RealDigital\GuestBook\Entity\PostEntity;

class PostRepository
{
    const TABLE_NAME = 'post';
    const COLUMN_ID = 'id';
    const COLUMN_TITLE = 'title';
    const COLUMN_TEXT = 'text';
    const COLUMN_USER_ID = 'user_id';
    const COLUMN_CREATE_TIME = 'create_time';

    /** @var DatabaseInterface $database */
    private $database;

    public function __construct(DatabaseInterface $database)
    {
        $this->database = $database;
    }

    public function deletePost(int $postId): void
    {
        $where = sprintf('`%s`=%s',self::COLUMN_ID, $postId);
        $this->database->deleteQuery(self::TABLE_NAME, $where);
    }


    public function getPostAmount(): int
    {
        return $this->database->count(self::TABLE_NAME);
    }

    /**
     * @return PostEntity[]
     */
    public function getAllPosts($limit, $offset): array
    {
        $order = 'BY `'.self::TABLE_NAME.'`.`'.self::COLUMN_ID.'` DESC';
        $posts = $this->database->selectQuery(self::TABLE_NAME, '*', '', $order, $limit, $offset);

        return $this->convertToEntityList($posts);
    }

    public function getPostById(int $id): ?PostEntity
    {
        $where = sprintf('%s = %s;',self::COLUMN_ID, $id);
        $columns = '*';
        $result = $this->database->selectQuery(self::TABLE_NAME, $columns, $where);

        if ($result === null) {
            return null;
        }

        return $this->convertToEntity($result);
    }

    public function save(PostEntity $post): bool
    {
        $params = [];
        if ($post->getPostId() !== null) {
            $postId = $post->getPostId();
            $params[self::COLUMN_ID] = $postId;
        }

        if ($post->getTitle() !== null) {
            $title = $this->database->escapeString($post->getTitle());
            $params[self::COLUMN_TITLE] = $title;
        }

        if ($post->getText() !== null) {
            $text = $this->database->escapeString($post->getText());
            $params[self::COLUMN_TEXT] = $text;
        }

        if ($post->getUserId() !== null) {
            $lastName = $post->getUserId();
            $params[self::COLUMN_USER_ID] = $lastName;
        }

        if ($post->getPostId() === null) {
            return $this->createPost($params);
        }

        return $this->editPost($post->getPostId(), $params);
    }

    private function createPost(array $params): bool
    {
        $postId = $this->database->insertQuery(self::TABLE_NAME, $params);

        if ($postId === null) {
            return false;
        }

        return true;
    }

    private function editPost(int $postId, array $params): bool
    {
        $where = sprintf("%s = %s", self::COLUMN_ID, $postId);
        return $this->database->updateQuery(self::TABLE_NAME, $params, $where);
    }


    private function convertToEntityList(?array $results): array
    {
        $posts = [];
        if ($results === null) {
            return $posts;
        }
        if (!isset($results[0])) {
            $posts[] = $this->convertToEntity($results);
            return $posts;
        }
        foreach ($results as $result) {
            $posts[] = $this->convertToEntity($result);
        }

        return $posts;
    }

    private function convertToEntity(array $result): ?PostEntity
    {
        $post = new postEntity();

        if ($result[self::COLUMN_ID] !== null) {
            $post->setPostId((int)$result[self::COLUMN_ID]);
        }
        if ($result[self::COLUMN_TITLE] !== null) {
            $post->setTitle($result[self::COLUMN_TITLE]);
        }
        if ($result[self::COLUMN_TEXT] !== null) {
            $post->setText($result[self::COLUMN_TEXT]);
        }
        if ($result[self::COLUMN_USER_ID] !== null) {
            $post->setUserId((int)$result[self::COLUMN_USER_ID]);
        }
        if ($result[self::COLUMN_CREATE_TIME] !== null) {
            $post->setCreateTime(strtotime($result[self::COLUMN_CREATE_TIME]));
        }

        return $post;
    }
}
