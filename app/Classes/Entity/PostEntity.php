<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Entity;

class PostEntity
{
    /** @var null|int */
    private $postId;

    /** @var null|string */
    private $title;

    /** @var null|string */
    private $text;

    /** @var null|int */
    private $userId;

    /** @var null|int */
    private $createTime;


    public function getPostId(): ?int
    {
        return $this->postId;
    }

    public function setPostId(int $postId): void
    {
        $this->postId = $postId;
    }

    function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function getCreateTime(): ?int
    {
        return $this->createTime;
    }

    public function setCreateTime(int $createTime): void
    {
        $this->createTime = $createTime;
    }
}