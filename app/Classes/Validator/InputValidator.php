<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Validator;

use RealDigital\GuestBook\Exeption\Input\InvalidArgumentException;
use RealDigital\GuestBook\Exeption\Input\MissingInputException;
use RealDigital\GuestBook\Service\HttpService;

class InputValidator
{
    //TODO tests
    /**
     * @throws InvalidArgumentException
     * @throws MissingInputException
     */
    public static function checkPostInput(string $input): string
    {
        $postInput = HttpService::post($input);

        if ($postInput === null) {
            throw new MissingInputException('Input ' . $input . ' is missing');
        }

        if ($postInput === '') {
            throw new InvalidArgumentException('Input ' . $input . ' is empty');
        }

        return $postInput;
    }
}