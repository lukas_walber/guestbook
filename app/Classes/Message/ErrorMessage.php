<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Message;

class ErrorMessage implements MessageInterface
{
    /** @var string */
    private $message = '';

    public function __construct(string $message)
    {
        $this->message = $message;
    }

    public function getMessage(): string
    {
        return '<div class="alert alert-danger" role="alert">' . $this->message .'</div>';
    }

    public function getType(): string
    {
        return MessageInterface::TYPE_ERROR;
    }

    public function toArray(): array
    {
        return [
            'text' => $this->message,
            'type' => $this->getType(),
        ];
    }
}