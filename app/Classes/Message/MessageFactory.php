<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Message;

class MessageFactory
{
    public static function getMessageFromArray(array $message): ?MessageInterface
    {
        $text = $message['text'] ?? '';
        $type = $message['type'] ?? '';

        switch ($type) {
            case MessageInterface::TYPE_INFO:
                return new InfoMessage($text);
            case MessageInterface::TYPE_ERROR:
                return new ErrorMessage($text);
            case MessageInterface::TYPE_WARNING:
                return new WarningMessage($text);
            case MessageInterface::TYPE_SUCCESS:
                return new SuccessMessage($text);
            default:
                return null;
        }
    }
}