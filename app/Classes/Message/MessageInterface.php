<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Message;

interface MessageInterface
{
    const TYPE_INFO = 'info';
    const TYPE_SUCCESS = 'success';
    const TYPE_WARNING = 'warning';
    const TYPE_ERROR = 'error';

    public function getMessage(): string;

    public function getType(): string;

    public function toArray(): array;
}