<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller;

use RealDigital\GuestBook\Message\MessageFactory;
use RealDigital\GuestBook\Service\SessionService;
use RealDigital\GuestBook\TemplateEngine\ViewInterface;

abstract class AbstractController
{
    /** @var ViewInterface */
    protected $view;

    /** @var SessionService */
    private $sessionService;

    public function __construct(ViewInterface $view, SessionService $sessionService)
    {
        $this->view = $view;
        $this->sessionService = $sessionService;
        $this->assignMessage();
    }

    private function assignMessage(): void
    {
        $sessionMessage = $this->sessionService->getMessage();

        if ($sessionMessage === null) {
            return;
        }

        $message = MessageFactory::getMessageFromArray($sessionMessage);

        if ($message === null) {
            return;
        }

        $this->view->assignSingle($message->getType(), $message->getMessage());
        $this->sessionService->resetMessage();
    }
}