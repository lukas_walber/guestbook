<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller\Auth;

use RealDigital\GuestBook\Controller\RequestActionInterface;
use RealDigital\GuestBook\Message\SuccessMessage;
use RealDigital\GuestBook\Service\HttpService;
use RealDigital\GuestBook\Service\SessionService;

class LogoutController implements RequestActionInterface
{
    /** @var SessionService */
    private $sessionService;

    private $httpService;

    public function __construct(SessionService $sessionService, HttpService $httpService)
    {
        $this->sessionService = $sessionService;
        $this->httpService = $httpService;
        $this->sessionService->startSession();
    }

    public function request(): void
    {
        $this->sessionService->resetUser();
        $this->sessionService->setLogin(false);
        $message = new SuccessMessage('logout successful!');
        $this->sessionService->setMessage($message);

        $this->httpService->redirectTo('login');
    }
}