<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller\Auth;

use RealDigital\GuestBook\Controller\AbstractController;
use RealDigital\GuestBook\Controller\RequestActionInterface;
use RealDigital\GuestBook\Controller\ShowActionInterface;
use RealDigital\GuestBook\Exeption\Input\InputException;
use RealDigital\GuestBook\Message\ErrorMessage;
use RealDigital\GuestBook\Message\WarningMessage;
use RealDigital\GuestBook\Repository\UserRepository;
use RealDigital\GuestBook\Service\HttpService;
use RealDigital\GuestBook\Service\SessionService;
use RealDigital\GuestBook\TemplateEngine\ViewInterface;
use RealDigital\GuestBook\Validator\InputValidator;

class LoginController extends AbstractController implements RequestActionInterface, ShowActionInterface
{
    /** @var UserRepository */
    private $userRepository;

    private $sessionService;

    private $httpService;

    public function __construct(
        UserRepository $userRepository,
        ViewInterface $view,
        SessionService $sessionService,
        HttpService $httpService
    )

    {
        parent::__construct($view, $sessionService);
        $this->userRepository = $userRepository;
        $this->sessionService = $sessionService;
        $this->httpService = $httpService;
        $this->sessionService->startSession();
    }

    public function show(): void
    {
        if ($this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('');
        }

        $this->view->setTemplate('login');
        $username = $this->httpService->get('username');

        if ($username !== null) {
            htmlentities($username);
            $this->view->assignSingle('username', $username);
        }

        echo $this->view->render();
    }

    public function request(): void
    {

        if ($this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('/');
        }
        $this->checkFailedLogins();
        $this->sessionService->resetSessionTimer();

        $redirectTo = 'login';

        try {
            $username = InputValidator::checkPostInput('username');
            $password = InputValidator::checkPostInput('password');
        } catch (InputException $e) {
            $error = new ErrorMessage($e->getMessage());
            $this->sessionService->setMessage($error);
            $url = $this->httpService->buildUrlWithGetValues($redirectTo, $username);

            $this->httpService->redirectTo($url);
        }

        if (!isset($username) || !isset($password)) {
            $message = new WarningMessage('username/password not set');
            $this->sessionService->setMessage($message);
            $url = $this->httpService->buildUrlWithGetValues($redirectTo, $username);

            $this->httpService->redirectTo($url);
        }

        $password = $this->userRepository->encryptPassword($password);

        $user = $this->userRepository->getUserByUsername($username);
        if ($user === null || $user->getPassword() !== $password || $user->getUsername() !== $username) {
            $message = new ErrorMessage('Wrong Username or Password !');
            $this->sessionService->setMessage($message);
            $this->sessionService->addFailedLogin();
            $url = $this->httpService->buildUrlWithGetValues($redirectTo, $username);

            $this->httpService->redirectTo($url);
        }

        $user = $this->userRepository->getUserByUsername($username);
        $this->sessionService->setLogin();
        $this->sessionService->setUserId($user->getId());

        $this->httpService->redirectTo('');
    }

    private function checkFailedLogins(): void
    {
        $failedLogins = $this->sessionService->checkFailedLogins();
        if ($failedLogins < 3) {
            return;
        }

        $untilReset = $this->sessionService->howLongUntilSessionReset();
        if ($untilReset >= 300) {
            return;
        }

        $untilReset = 300 - $untilReset;
        $message = new ErrorMessage('To many failed logins, please try again in ' . $untilReset . ' seconds');
        $this->sessionService->setMessage($message);

        $this->httpService->redirectTo('login');
    }
}