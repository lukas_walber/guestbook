<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller\Auth;

use RealDigital\GuestBook\Controller\AbstractController;
use RealDigital\GuestBook\Entity\UserEntity;
use RealDigital\GuestBook\Exeption\Input\InputException;
use RealDigital\GuestBook\Mailer\StandardMailer;
use RealDigital\GuestBook\Message\ErrorMessage;
use RealDigital\GuestBook\Message\SuccessMessage;
use RealDigital\GuestBook\Repository\UserRepository;
use RealDigital\GuestBook\Service\HttpService;
use RealDigital\GuestBook\Service\SessionService;
use RealDigital\GuestBook\TemplateEngine\ViewInterface;
use RealDigital\GuestBook\Validator\InputValidator;

class RegistrationController extends AbstractController
{
    /** @var UserRepository */
    private $userRepository;

    /** @var SessionService */
    private $sessionService;

    private $httpService;

    public function __construct(
        UserRepository $userRepository,
        ViewInterface $view,
        SessionService $sessionService,
        HttpService $httpService
    )

    {
        parent::__construct($view, $sessionService);
        $this->userRepository = $userRepository;
        $this->sessionService = $sessionService;
        $this->httpService = $httpService;
        $this->sessionService->startSession();
    }

    public function show(): void
    {
        if ($this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('');
        }

        $this->view->setTemplate('registration');

        $username = $this->httpService->get('username');
        if ($username !== null) {
            htmlentities($username);
            $this->view->assignSingle('username', $username);
        }

        $email = $this->httpService->get('email');
        if ($email !== null) {
            htmlentities($email);
            $this->view->assignSingle('email', $email);
        }

        $firstName = $this->httpService->get('first_name');
        if ($firstName !== null) {
            htmlentities($firstName);
            $this->view->assignSingle('first_name', $firstName);
        }

        $lastName = $this->httpService->get('last_name');
        if ($lastName !== null) {
            htmlentities($lastName);
            $this->view->assignSingle('last_name', $lastName);
        }

        echo $this->view->render();
    }

    public function request(): void
    {
        if ($this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('');
        }

        $redirectTo = 'registration';
        try {
            $username = InputValidator::checkPostInput('username');
            $password = InputValidator::checkPostInput('password');
            $passwordConfirm = InputValidator::checkPostInput('password_confirm');
            $firstName = InputValidator::checkPostInput('first_name');
            $lastName = InputValidator::checkPostInput('last_name');
            $email = InputValidator::checkPostInput('email');
        } catch (InputException $e) {
            $error = new ErrorMessage($e->getMessage());
            $this->sessionService->setMessage($error);
            $url = $this->httpService->buildUrlWithGetValues($redirectTo, $username, $email, $firstName, $lastName);

            $this->httpService->redirectTo($url);
        }

        $url = $this->httpService->buildUrlWithGetValues($redirectTo, $username, $email, $firstName, $lastName);

        $test = $this->passwordStrengthCheck($password);
        if ($test === false) {
            $message = new ErrorMessage('Password not strong enough!');
            $this->sessionService->setMessage($message);
            $this->httpService->redirectTo($url);
        }

        $test = $this->passwordLengthCheck($password);
        if ($test === false) {
            $message = new ErrorMessage('Password not long enough!');
            $this->sessionService->setMessage($message);
            $this->httpService->redirectTo($url);
        }

        $usernameCheck = $this->userRepository->getUserByUsername($username);
        if ($usernameCheck !== null) {
            $message = new ErrorMessage('Username already in use');
            $this->sessionService->setMessage($message);
            $this->httpService->redirectTo($url);
        }

        $emailCheck = $this->userRepository->getUserByEmail($email);
        if ($emailCheck !== null) {
            $message = new ErrorMessage('Email is already in use!');
            $this->sessionService->setMessage($message);
            $this->httpService->redirectTo($url);
        }

        if ($password !== $passwordConfirm) {
            $message = new ErrorMessage('Passwords don\'t match !');
            $this->sessionService->setMessage($message);
            $this->httpService->redirectTo($url);
        }

        $password = $this->userRepository->encryptPassword($password);

        $user = new UserEntity();

        $user->setUsername($username);
        $user->setPassword($password);
        $user->setEmail($email);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);

        $insertUser = $this->userRepository->save($user);
        if ($insertUser === null) {
            $message = new ErrorMessage('Account couldn\'t be created');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo($url);
        }

        $mailSendSuccessfully = $this->sendVerificationMail($email);
        if ($mailSendSuccessfully === false) {
            $message = new ErrorMessage('verification email couldn\'t be send');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo($url);
        }
        $message = new SuccessMessage('please confirm your email !');
        $this->sessionService->setMessage($message);

        $this->httpService->redirectTo('');
    }

    private function passwordStrengthCheck(string $password): bool
    {
        $passwordStrength = 0;
        $patterns = [
            '/[A-Z]/',   //Upper letter
            '/[a-z]/',   //Lower letter
            '/\d/',      //digit
            '/\W/',      //special chars
        ];

        foreach ($patterns as $pattern) {
            $match = preg_match($pattern, $password);
            if ($match === 0) {
                continue;
            }
            $passwordStrength++;
        }

        if ($passwordStrength < 3) {
            return false;
        }

        return true;
    }

    private function passwordLengthCheck(string $password): bool
    {
        if (strlen($password) <= 7) {
            return false;
        }

        return true;
    }

    private function sendVerificationMail(string $email): bool
    {
        $user = $this->userRepository->getUserByEmail($email);
        if ($user === null) {
            return false;
        }

        $subject = 'Verification E-Mail';
        $emailFrom = 'Guestbook@GuestBook.com';

        $href = sprintf(
            'http://%s/verifycation?id%s&verify_hash%s',
            $_SERVER['HTTP_HOST'],
            '=' . $user->getId(),
            '=' . $user->getVerifyHash()
        );
        $this->view->setTemplate('mail');
        $this->view->assignSingle('username', $user->getUsername());
        $this->view->assignSingle('href', $href);
        $htmlBodyMail = $this->view->render();

        $mailSendSuccessfully = StandardMailer::sendHtmlMail($email, $emailFrom, $subject, $htmlBodyMail);
        if ($mailSendSuccessfully === false) {
            return false;
        }

        return true;
    }
}