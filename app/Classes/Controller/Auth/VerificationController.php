<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller\Auth;

use RealDigital\GuestBook\Controller\AbstractController;
use RealDigital\GuestBook\Controller\RequestActionInterface;
use RealDigital\GuestBook\Message\ErrorMessage;
use RealDigital\GuestBook\Message\SuccessMessage;
use RealDigital\GuestBook\Message\WarningMessage;
use RealDigital\GuestBook\Repository\UserRepository;
use RealDigital\GuestBook\Service\HttpService;
use RealDigital\GuestBook\Service\SessionService;
use RealDigital\GuestBook\TemplateEngine\ViewInterface;

class VerificationController extends AbstractController implements RequestActionInterface
{
    /** @var UserRepository */
    private $userRepository;

    /** @var SessionService */
    private $sessionService;

    private $httpService;

    public function __construct(
        UserRepository $userRepository,
        ViewInterface $view,
        SessionService $sessionService,
        HttpService $httpService
    )
    {
        parent::__construct($view, $sessionService);
        $this->userRepository = $userRepository;
        $this->sessionService = $sessionService;
        $this->httpService = $httpService;
        $this->sessionService->startSession();
    }

    public function request(): void
    {
        if ($this->sessionService->checkLogin()) {
            $message = new WarningMessage('you are already verified');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('');
        }

        $verifyHash = $this->httpService->get('verify_hash');
        $id = $this->httpService->get('id');

        if ($verifyHash === null || $id === null) {
            $message = new WarningMessage('no verification id/hash');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('login');
        }

        $user = $this->userRepository->getUserById((int)$id);

        if ($user === null) {
            $message = new WarningMessage('user id not found');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('login');
        }

        if ($user->isVerified() === true) {
            $message = new WarningMessage('user is already verified');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('login');
        }

        if ($user->getVerifyHash() !== $verifyHash) {
            $message = new WarningMessage('wrong verification hash');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('login');
        }

        $user->setVerified(true);

        $verify = $this->userRepository->save($user);

        if ($verify === false) {
            $message = new ErrorMessage('something went wrong, please try again later');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('login');
        }

        $message = new SuccessMessage('your account has been verified');
        $this->sessionService->setMessage($message);

        $this->httpService->redirectTo('');
    }
}