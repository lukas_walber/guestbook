<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller;

interface RequestActionInterface
{
    public function request(): void;
}