<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller\Forum;

use RealDigital\GuestBook\Controller\AbstractController;
use RealDigital\GuestBook\Controller\ShowActionInterface;
use RealDigital\GuestBook\Repository\PostRepository;
use RealDigital\GuestBook\Repository\UserRepository;
use RealDigital\GuestBook\Service\HttpService;
use RealDigital\GuestBook\Service\SessionService;
use RealDigital\GuestBook\TemplateEngine\View;
use RealDigital\GuestBook\TemplateEngine\ViewInterface;

class GuestbookController extends AbstractController implements ShowActionInterface
{
    const LIMIT = 10;

    /** @var UserRepository */
    private $userRepository;

    /** @var PostRepository */
    private $postRepository;

    /** @var SessionService */
    private $sessionService;

    private $httpService;

    public function __construct(
        UserRepository $userRepository,
        PostRepository $postRepository,
        ViewInterface $view,
        SessionService $sessionService,
        HttpService $httpService
    )
    {
        parent::__construct($view, $sessionService);
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->sessionService = $sessionService;
        $this->httpService = $httpService;
        $this->sessionService->startSession();
    }

    public function show(): void
    {
        if (!$this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('login');
        }

        $this->view->setTemplate('index');
        $user = $this->userRepository->getUserById($this->sessionService->getUserId());

        $offset = 0;
        $currentPage = 1;

        $postAmount = $this->postRepository->getPostAmount();
        $numberOfPages = (int) ceil($postAmount / self::LIMIT);

        $getParamPage = $this->httpService->get('page');
        if ($getParamPage !== null && $getParamPage > 1 && $getParamPage <= $numberOfPages) {
            $currentPage = (int) $getParamPage;
            $offset = ($currentPage * self::LIMIT) - self::LIMIT;
        }

        $posts = $this->renderPosts($offset);
//        $pagination = $this->renderPagination($currentPage, $numberOfPages);
        $this->view->setTemplate('index');
//        $this->view->assignSingle('pagination', $pagination);
        $this->view->assignSingle('posts', $posts);
        $this->view->assignSingle('user', $user->getUsername());
        $html = $this->view->render();

        echo $html;
    }

    private function renderPosts($offset): string
    {
        if (!$this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('login');
        }

        $posts = $this->postRepository->getAllPosts(self::LIMIT, $offset);

        $postsString = '';

        $this->view->setTemplate('post');

        foreach ($posts as $post) {
            $user = $this->userRepository->getUserById($post->getUserId());

            $date = new \DateTime();
            $date->setTimestamp($post->getCreateTime());

            $posts = [
                'username' => $user->getUsername(),
                'user_full_name' => $user->getFirstName(). ' ' .$user->getLastName(),
                'user_photo' => $user->getPhoto() ?? 'resources/public/image/face.png',
                'post_date' => $date->format('d.m.Y'),
                'post_time' => $date->format('H:i'),
                'post_title' => $post->getTitle(),
                'post_text' => $post->getText(),
            ];

            $this->view->assignMulti($posts);
            $postsString .= $this->view->render(false);
        }

        return $postsString;
    }

    private function renderPagination($currentPage, $numberOfPages): string
    {
        if ($numberOfPages < 1) {
            return '';
        }

        $paginationString = '';

        $this->view->setTemplate('pagination');

        for ($i = 1; $i <= $numberOfPages; $i++) {
            $pagination = [
                'page' => $i,
                'href' => $i,
            ];
            if ($i === $currentPage) {
                $this->view->setTemplate('pagination');
                $pagination['active'] = 'active';
                $this->view->assignMulti($pagination);
                $paginationString .= $this->view->render();
                continue;
            }
            $this->view->assignMulti($pagination);
            $paginationString .= $this->view->render(false);
            $pagination['active'] = '';
        }
        $this->view->setTemplate('paginationComplete');
        $this->view->assignSingle('pagination', $paginationString);

        return $this->view->render();
    }
}