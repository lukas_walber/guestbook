<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller\Forum;

use RealDigital\GuestBook\Controller\AbstractController;
use RealDigital\GuestBook\Controller\ShowActionInterface;
use RealDigital\GuestBook\Message\ErrorMessage;
use RealDigital\GuestBook\Repository\UserRepository;
use RealDigital\GuestBook\Service\HttpService;
use RealDigital\GuestBook\Service\SessionService;
use RealDigital\GuestBook\TemplateEngine\ViewInterface;

class ProfileController extends AbstractController implements ShowActionInterface
{
    /** @var UserRepository */
    private $userRepository;

    /** @var SessionService */
    private $sessionService;

    private $httpService;


    public function __construct(
        UserRepository $userRepository,
        ViewInterface $view,
        SessionService $sessionService,
        HttpService $httpService
    )
    {
        parent::__construct($view, $sessionService);
        $this->userRepository = $userRepository;
        $this->sessionService = $sessionService;
        $this->httpService = $httpService;
        $this->sessionService->startSession();
    }

    public function show(): void
    {
        if (!$this->checkLogin()) {
            $this->httpService->redirectTo('login');
        }

        $username = $this->httpService->get('username');
        $this->view->setTemplate('profile');

        if ($username === null || $username === '') {
            $message = new ErrorMessage('User doesn\'t exist');
            $this->setMessage($message);
            $this->httpService->redirectTo('');
        }

        $user = $this->userRepository->getUserByUsername($username);
        $username = $this->userRepository->getUserById($this->getUserId());

        if ($username->getUsername() === $user->getUsername()) {
            $this->httpService->redirectTo('user/settings');
        }

        if ($user === null) {
            $message = new ErrorMessage('User doesn\'t exist');
            $this->setMessage($message);
            $this->httpService->redirectTo('');
        }

        $assignArray = [
            'photo' => $user->getPhoto() ?? 'resources/public/image/face.png',
            'username' => $user->getUsername(),
            'firstname' => $user->getFirstName(),
            'lastname' => $user->getLastName(),
            'email' => $user->getEmail(),
        ];

        $this->view->assignMulti($assignArray);
        $html = $this->view->render();

        echo $html;
    }
}