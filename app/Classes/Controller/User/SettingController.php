<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller\User;

use RealDigital\GuestBook\Controller\AbstractController;
use RealDigital\GuestBook\Controller\RequestActionInterface;
use RealDigital\GuestBook\Controller\ShowActionInterface;
use RealDigital\GuestBook\Entity\UserEntity;
use RealDigital\GuestBook\Exception\UploadException;
use RealDigital\GuestBook\Exeption\Input\InputException;
use RealDigital\GuestBook\Message\ErrorMessage;
use RealDigital\GuestBook\Message\SuccessMessage;
use RealDigital\GuestBook\Message\WarningMessage;
use RealDigital\GuestBook\Repository\UserRepository;
use RealDigital\GuestBook\Service\HttpService;
use RealDigital\GuestBook\Service\ImageService;
use RealDigital\GuestBook\Service\SessionService;
use RealDigital\GuestBook\TemplateEngine\ViewInterface;
use RealDigital\GuestBook\Validator\InputValidator;

class SettingController extends AbstractController implements ShowActionInterface, RequestActionInterface
{
    /** @var UserRepository */
    private $userRepository;

    /** @var SessionService */
    private $sessionService;

    private $httpService;

    private $imageService;

    public function __construct(
        UserRepository $userRepository,
        ViewInterface $view,
        SessionService $sessionService,
        HttpService $httpService,
        ImageService $imageService
    )
    {
        parent::__construct($view, $sessionService);
        $this->userRepository = $userRepository;
        $this->sessionService = $sessionService;
        $this->httpService = $httpService;
        $this->imageService = $imageService;
        $sessionService->startSession();
    }

    public function show(): void
    {
        if (!$this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('login');
        }

        $this->view->setTemplate('userSettings');
        $user = $this->userRepository->getUserById($this->sessionService->getUserId());
        $arguments = [
            'photo' => '../' . $user->getPhoto() ?? '../resources/public/image/face.png',
            'username' => $user->getUsername(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'email' => $user->getEmail(),
        ];

        $this->view->assignMulti($arguments);

        echo $this->view->render();
    }

    public function request(): void
    {
        if (!$this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('login');
        }

        try {
            $type = InputValidator::checkPostInput('type');
        } catch (InputException $e) {
            $error = new ErrorMessage('unknown request');
            $this->sessionService->setMessage($error);

            $this->httpService->redirectTo('user/settings');
        }

        switch ($type) {
            case 'userPhoto':
                $this->updatePhoto();
                break;
            case 'userInfo':
                $this->updateInfo();
                break;
            case 'userPassword':
                $this->updatePassword();
                break;
        }

        $this->httpService->redirectTo('user/settings');
    }

    private function updatePhoto(): void
    {
        try {
            $pathToPicture = $this->imageService->uploadPicture();
        } catch (UploadException $e) {
            $message = new ErrorMessage($e->getMessage());
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('user/settings');
        }

        $user = new UserEntity();
        $user->setPhoto($pathToPicture);
        $user->setId($this->sessionService->getUserId());
        $update = $this->userRepository->save($user);
        if ($update === false) {
            $message = new WarningMessage('something went wrong, please try again in a minute!');
            $this->sessionService->setMessage($message);

            return;
        }

        $message = new SuccessMessage('picture saved');
        $this->sessionService->setMessage($message);
    }

    private function updateInfo()
    {
        try {
            $username = InputValidator::checkPostInput('username');
            $firstName = InputValidator::checkPostInput('first_name');
            $lastName = InputValidator::checkPostInput('last_name');
            $email = InputValidator::checkPostInput('email');
        } catch (InputException $e) {
            $error = new ErrorMessage($e->getMessage());
            $this->sessionService->setMessage($error);

            $this->httpService->redirectTo('user/settings');
        }

        $userFromEmail = $this->userRepository->getUserByEmail($email);
        if ($userFromEmail !== null && (int) $userFromEmail->getId() !== SessionService::getUserId()) {
            $message = new ErrorMessage('Email is already in use!');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('user/settings');
        }

        $userFromUsername = $this->userRepository->getUserByUsername($username);
        if ($userFromUsername !== null && (int) $userFromUsername->getId() !== SessionService::getUserId()) {
            $message = new ErrorMessage('Username already in use');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('user/settings');
        }

        $userId = $this->sessionService->getUserId();
        $user = $this->userRepository->getUserById($userId);

        $user->setUsername($username);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setEmail($email);

        $update = $this->userRepository->save($user);

        if ($update === false) {
            $message = new WarningMessage('something went wrong, please try again in a minute!');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('user/settings');
        }

        $message = new SuccessMessage('Changes have been saved !');
        $this->sessionService->setMessage($message);

        $this->httpService->redirectTo('');
    }

    private function updatePassword()
    {
        try {
            $oldPassword = InputValidator::checkPostInput('password');
            $newPassword = InputValidator::checkPostInput('password_1');
            $newPasswordConfirm = InputValidator::checkPostInput('password_2');
        } catch (InputException $e) {
            $error = new ErrorMessage($e->getMessage());
            $this->sessionService->setMessage($error);

            $this->httpService->redirectTo('user/settings');
        }

        $user = $this->userRepository->getUserById($this->sessionService->getUserId());
        $password = $this->userRepository->encryptPassword($oldPassword);

        if ($password !== $user->getPassword()) {
            $message = new ErrorMessage('Wrong Password!');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('user/settings');
        }

        if ($newPassword !== $newPasswordConfirm) {
            $message = new ErrorMessage('Passwords don\'t match!');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('user/settings');
        }

        $user->setPassword(md5($newPassword));
        $update = $this->userRepository->save($user);

        if ($update === false) {
            $message = new WarningMessage('something went wrong, please try again in a minute!');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('user/settings');
        }

        $message = new SuccessMessage('Changes have been saved !');
        $this->sessionService->setMessage($message);

        $this->httpService->redirectTo('');
    }
}