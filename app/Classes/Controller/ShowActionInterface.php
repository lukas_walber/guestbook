<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller;

interface ShowActionInterface
{
    public function show(): void;
}