<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Controller\Post;

use RealDigital\GuestBook\Controller\AbstractController;
use RealDigital\GuestBook\Controller\RequestActionInterface;
use RealDigital\GuestBook\Entity\PostEntity;
use RealDigital\GuestBook\Exeption\Input\InputException;
use RealDigital\GuestBook\Message\ErrorMessage;
use RealDigital\GuestBook\Message\SuccessMessage;
use RealDigital\GuestBook\Repository\PostRepository;
use RealDigital\GuestBook\Repository\UserRepository;
use RealDigital\GuestBook\Service\HttpService;
use RealDigital\GuestBook\Service\SessionService;
use RealDigital\GuestBook\TemplateEngine\ViewInterface;
use RealDigital\GuestBook\Validator\InputValidator;

class PostController extends AbstractController implements RequestActionInterface
{
    /** @var UserRepository */
    private $postRepository;

    /** @var SessionService */
    private  $sessionService;

    private $httpService;

    public function __construct(
        PostRepository $postRepository,
        ViewInterface $view,
        SessionService $sessionService,
        HttpService $httpService
    )
    {
        parent::__construct($view, $sessionService);
        $this->postRepository = $postRepository;
        $this->sessionService = $sessionService;
        $this->httpService = $httpService;
        $this->sessionService->startSession();
    }

    public function request(): void
    {
        if (!$this->sessionService->checkLogin()) {
            $this->httpService->redirectTo('login');
        }

        try {
            $title = InputValidator::checkPostInput('title');
            $text = InputValidator::checkPostInput('text');
        } catch (InputException $e) {
            $message = new ErrorMessage($e->getMessage());
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('');
        }

        $post = new PostEntity();
        $post->setTitle($title);
        $post->setText($text);
        $post->setUserId($this->sessionService->getUserId());

        $insertPost = $this->postRepository->save($post);

        if ($insertPost === null) {
            $message = new ErrorMessage('upload failed, please try again!');
            $this->sessionService->setMessage($message);

            $this->httpService->redirectTo('');
        }

        $message = new SuccessMessage('Post saved !');
        $this->sessionService->setMessage($message);

        $this->httpService->redirectTo('');
    }
}