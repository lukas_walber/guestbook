-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Erstellungszeit: 21. Nov 2018 um 13:35
-- Server-Version: 5.7.23
-- PHP-Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Datenbank: `guest_book`
--

CREATE DATABASE IF NOT EXISTS guest_book;

USE guest_book;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `post`
--

INSERT INTO `post` (`id`, `title`, `text`, `user_id`, `create_time`) VALUES
(1, 'Test', 'Test Post', 1, '2018-10-09 12:51:54'),
(2, 'd1121d2', 'd12d1d1d2', 1, '2018-10-23 10:04:00'),
(3, 'd1121d2', 'd12d1d1d2', 1, '2018-10-23 10:04:17'),
(4, '5asd46a14s6d4', 'as6d41as4das', 1, '2018-10-23 10:04:28'),
(5, 'hallo', 'alkfjsaoisnuoidunscpoiauencsenoircaosuenufasiouenvafuesnouansieaoiuevfuosdnuvoasuoidvoasnuodfuvoasuodfuvoinusdfuvsuopdufnpsvoidufnpaosidufnposaiudfnv\r\n', 1, '2018-10-25 14:50:39'),
(6, 'werewrwer', 'werwerewrwr', 1, '2018-10-31 12:04:07'),
(14, 'asdasd2', '2313123214', 1, '2018-10-31 12:45:50'),
(15, '4124124qw', 'dasdasdads', 1, '2018-10-31 12:45:53'),
(16, 'dwq21121d', 'd1212d12rr12', 1, '2018-10-31 12:45:56'),
(17, '12e12e12e', 'asdasfasgasg', 1, '2018-10-31 12:46:00'),
(18, 'dawdadwad', 'w1d1211212d', 1, '2018-10-31 12:46:03'),
(19, '8948646848', 'hjhhkjhjkhjh\r\n', 1, '2018-10-31 14:51:27'),
(20, 'test', 'testtest\r\n', 1, '2018-11-12 12:00:37'),
(21, 'testtest', 'test', 1, '2018-11-14 13:25:30'),
(22, 'bot', 'bot\r\n', 2, '2018-11-16 09:26:16');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `verify_hash` varchar(255) DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `photo` varchar(255) DEFAULT NULL,
  `role` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `username`, `first_name`, `last_name`, `email`, `password`, `verify_hash`, `verified`, `photo`, `role`) VALUES
(1, 'admin', 'admin', 'admin', 'admin@admin.de', '21232f297a57a5a743894a0e4a801fc3', '5bbb173792cd4', 0, 'resources/public/image/5beea9f305d589.67083081_kphenojlfmigq.jpeg', 1),
(2, 'bot', 'bot', 'bot', 'bot@bot.de', 'fabcaa97871555b68aa095335975e613', 'fabcaa97871555b68aa095335975e613', 1, NULL, 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `username` (`username`,`email`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
