<?php

namespace RealDigital\GuestBook\Tests\integration\Repository;

use PHPUnit\Framework\TestCase;
use RealDigital\GuestBook\Connection\Database;
use RealDigital\GuestBook\Connection\DatabaseInterface;
use RealDigital\GuestBook\Entity\UserEntity;
use RealDigital\GuestBook\Repository\UserRepository;

class UserRepositoryTest extends TestCase
{
    const TABLE_NAME = 'user';
    const COLUMN_ID = 'id';
    const COLUMN_USERNAME = 'username';
    const COLUMN_PASSWORD = 'password';
    const COLUMN_EMAIL = 'email';
    const COLUMN_FIRST_NAME = 'first_name';
    const COLUMN_LAST_NAME = 'last_name';
    const COLUMN_VERIFY_HASH = 'verify_hash';
    const COLUMN_VERIFIED = 'verified';
    const COLUMN_PHOTO = 'photo';
    const COLUMN_ROLE = 'role';

    /** @var DatabaseInterface */
    private $database;

    public function setUp()
    {
        $this->database = new Database(
            null,
            null,
            '127.0.0.1',
            null,
            'test'
        );
    }

    public function tearDown()
    {
        $userRepository = new UserRepository($this->database);
        $users = $userRepository->getAllUsers();
        if ($users === null) {
            return;
        }

        foreach ($users as $user) {
            /** @var UserEntity $user */
            $userRepository->deleteUser($user->getId());
        }
    }

    public function test_NewUserCreated_When_SavingUserWithoutId()
    {
        ###insert test
        $userRepository = new UserRepository($this->database);
        $password = $userRepository->encryptPassword('123');

        $user = $this->setUser($password);

        $insertedUser = $userRepository->save($user);

        if ($insertedUser === null) {
            $this->fail('can not insert user');
        }

        $this->assertSame($user->getUsername(), $insertedUser->getUsername());
        $this->assertSame($user->getPassword(), $insertedUser->getPassword());
        $this->assertSame($user->getEmail(), $insertedUser->getEmail());
        $this->assertSame($user->getFirstName(), $insertedUser->getFirstName());
        $this->assertSame($user->getLastName(), $insertedUser->getLastName());
        $this->assertSame($user->getRole(), $insertedUser->getRole());
        $this->assertTrue(strlen($insertedUser->getVerifyHash()) === 13);
    }

    public function test_SaveReturnsNull_When_SavingAnEmptyUser()
    {
        ###insert test
        $userRepository = new UserRepository($this->database);

        $user = new UserEntity();

        $insertedUser = $userRepository->save($user);

        $this->assertNull($insertedUser);
    }

    public function test_SaveReturnsNull_When_TryingToEditANonExistingUser()
    {
        ###insert test
        $userRepository = new UserRepository($this->database);

        $user = new UserEntity();
        $user->setId(100);

        $insertedUser = $userRepository->save($user);

        $this->assertNull($insertedUser);
    }

    public function test_EditExistingUse_When_UsingSaveFunctionOnAnUserWithId()
    {
        ###editUser
        $userRepository = new UserRepository($this->database);
        $password = $userRepository->encryptPassword('123');
        $user = $this->setUser($password);

        $insertedUser = $userRepository->save($user);

        $insertedUser->setUsername('test2');

        $updatedUser = $userRepository->save($user);

        if ($updatedUser === null) {
            $this->fail('can not edit user');
        }

        $this->assertSame('test2', $updatedUser->getUsername());
    }


    public function test_getUserByUserNameReturnsNull_When_UsingAnUserNameThatDoesNotExist()
    {
        ###getUserbyUsernameFail
        $userRepository = new UserRepository($this->database);
        $user = $userRepository->getUserByUsername('userThatDoesNotExist');

        $this->assertNull($user);
    }

    public function test_getUserByEmailNameReturnsNull_When_UsingAnEmailThatDoesNotExist()
    {
        ###getUserByEmailFail
        $userRepository = new UserRepository($this->database);
        $user = $userRepository->getUserByEmail('userThatDoNotExist');

        $this->assertNull($user);
    }

    public function test_getUserByIdNameReturnsNull_When_UsingAnIdThatDoesNotExist()
    {
        ###getUserByIdFail

        $userRepository = new UserRepository($this->database);
        $user = $userRepository->getUserById(12365484);

        $this->assertNull($user);
    }

    public function test_RightUserReturned_When_GetUserByEmailUsed()
    {
        $userRepository = new UserRepository($this->database);
        $password = $userRepository->encryptPassword('123');

        $user = $this->setUser($password);

        $insertedUser = $userRepository->save($user);

        $user = $userRepository->getUserByEmail('test@test.de');

        $this->assertSame($insertedUser->getId(), $user->getId());
    }

    public function test_RightUserReturned_When_GetUserByUsernameUsed()
    {
        $userRepository = new UserRepository($this->database);
        $password = $userRepository->encryptPassword('123');

        $user = $this->setUser($password);

        $insertedUser = $userRepository->save($user);

        $user = $userRepository->getUserByUsername('test');

        $this->assertSame($insertedUser->getId(), $user->getId());
    }

    public function test_RightUserReturned_When_GetUserById()
    {
        $userRepository = new UserRepository($this->database);

        $user = $this->setUser(123);

        $insertedUser = $userRepository->save($user);

        $user = $userRepository->getUserByid($insertedUser->getId());

        $this->assertSame($insertedUser->getId(), $user->getId());;
    }

    public function test_ReturnNull_When_UsingGetAllUsersWithoutUsers()
    {
        $userRepository = new UserRepository($this->database);
        $allUsersTest = $userRepository->getAllUsers();

        $this->assertNull($allUsersTest);
    }

    public function test_ReturnOneUserEntity_When_UsingGetAllUsersWithOneUser()
    {
        $userRepository = new UserRepository($this->database);
        $password = $userRepository->encryptPassword('123');

        $user = $this->setUser($password);

        $userRepository->save($user);

        $userRepository = new UserRepository($this->database);
        $allUsersTest = $userRepository->getAllUsers();

        $this->assertCount(1, $allUsersTest);

        $this->assertSame($user->getId(), $allUsersTest[0]->getId());
    }

    public function test_ReturnMoreThanOneUserEntity_When_UsingGetAllUsersWithMoreThanOneUser()
    {
        $userRepository = new UserRepository($this->database);
        $password = $userRepository->encryptPassword('123');

        $user = $this->setUser($password);

        $userRepository->save($user);

        $user2 = new UserEntity();
        $user2->setUsername('test1');
        $user2->setPassword($password);
        $user2->setEmail('test@test.de2');
        $user2->setFirstName('firsttests2');
        $user2->setLastName('lasttest2');
        $user2->setPhoto('test2');

        $userRepository->save($user2);

        $userRepository = new UserRepository($this->database);
        $allUsersTest = $userRepository->getAllUsers();

        $this->assertCount(2, $allUsersTest);

        $this->assertSame($user->getId(), $allUsersTest[0]->getId());

        $this->assertSame($user2->getId(), $allUsersTest[1]->getId());
    }

    private function setUser($password, $id = null): UserEntity
    {
        $user = new UserEntity();
        if ($id !==null) {
            $user->setId(1);
        }

        $user->setUsername('test');
        $user->setPassword($password);
        $user->setEmail('test@test.de');
        $user->setFirstName('firsttests');
        $user->setLastName('lasttest');
        $user->setPhoto('test');
        $user->setRole(1);
        return $user;
    }
}
