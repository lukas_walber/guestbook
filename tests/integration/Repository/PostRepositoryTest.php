<?php

namespace RealDigital\GuestBook\Tests\integration\Repository;

use RealDigital\GuestBook\Connection\Database;
use RealDigital\GuestBook\Connection\DatabaseInterface;
use RealDigital\GuestBook\Entity\PostEntity;
use RealDigital\GuestBook\Repository\PostRepository;
use PHPUnit\Framework\TestCase;

class PostRepositoryTest extends TestCase
{
    const TABLE_NAME = 'post';
    const COLUMN_ID = 'id';
    const COLUMN_TITLE = 'title';
    const COLUMN_TEXT = 'text';
    const COLUMN_USER_ID = 'user_id';
    const COLUMN_CREATE_TIME = 'create_time';

    /** @var DatabaseInterface */
    private $database;

    public function setUp()
    {
        $this->database = new Database(
            null,
            null,
            '127.0.0.1',
            null,
            'test'
        );
    }

    public function tearDown()
    {
        $postRepository = new PostRepository($this->database);
        $posts = $postRepository->getAllPosts(100000, 0);
        if ($posts === null) {
            return;
        }
        foreach ($posts as $post) {
            /** @var PostEntity $post */
            $postRepository->deletePost($post->getPostId());
        }
    }

    public function test_OnePostInserted_When_SavingAPostWithoutId()
    {
        $postRepository = new PostRepository($this->database);
        $post = $this->setPostEntity();
        $insertTest = $postRepository->save($post);
        $this->assertTrue($insertTest);
    }

    public function test_PostNotInserted_When_SavingAPostWithoutAnyValues()
    {
        $postRepository = new PostRepository($this->database);
        $post = new PostEntity();
        $insertTest = $postRepository->save($post);
        $this->assertTrue(!$insertTest);
    }

    public function test_PostIsEdited_When_SavingAnPostWithId()
    {
        $postRepository = new PostRepository($this->database);
        $post = $this->setPostEntity();
        $postRepository->save($post);
        $post = $postRepository->getAllPosts(100, 0);
        $post = $post[0];
        $post->setTitle('test2');
        $post->setText('test2');
        $post->setUserId(2);
        $postRepository->save($post);
        $editedPost = $postRepository->getAllPosts(100, 0);
        $editedPost = $editedPost[0];

        $this->assertSame('test2', $editedPost->getTitle());
        $this->assertSame('test2', $editedPost->getText());
        $this->assertSame(2, $editedPost->getUserId());


    }

    public function test_NumberOfPostsReturned_When_CountingThreePosts()
    {
        $postRepository = new PostRepository($this->database);
        $post = $this->setPostEntity();
        $postRepository->save($post);
        $postRepository->save($post);
        $postRepository->save($post);
        $this->assertSame(3, $postRepository->getPostAmount());
    }

    public function test_ThatPostIsDeleted_When_UsingDeletePost()
    {
        $postRepository = new PostRepository($this->database);
        $post = $this->setPostEntity();
        $postRepository->save($post);
        $posts = $postRepository->getAllPosts(100,0);
        $postRepository->deletePost($posts[0]->getPostId());
        $this->assertSame(0, $postRepository->getPostAmount());
    }

    public function test_RightPostReturned_When_UsingGetPostById()
    {
        $postRepository = new PostRepository($this->database);
        $post = $this->setPostEntity();
        $postRepository->save($post);
        $insertedPost = $postRepository->getAllPosts(100, 0);
        $insertedPost = $postRepository->getPostById($insertedPost[0]->getPostId());

        $this->assertSame($post->getTitle(), $insertedPost->getTitle());
        $this->assertSame($post->getText(), $insertedPost->getText());
        $this->assertSame($post->getUserId(), $insertedPost->getUserId());
        $this->assertTrue(strlen($insertedPost->getCreateTime()) === 10);
    }

    public function test_NullReturned_When_UsingGetPostIdOnANonExistingId()
    {
        $postRepository = new PostRepository($this->database);
        $getPostByIdTest = $postRepository->getPostById(500);
        $this->assertNull($getPostByIdTest);

    }

    private function setPostEntity() :PostEntity
    {
        $post = new PostEntity();
        $post->setTitle('test');
        $post->setText('test');
        $post->setUserId(1);
        $post->setCreateTime(1545298479);
        return $post;
    }
}
