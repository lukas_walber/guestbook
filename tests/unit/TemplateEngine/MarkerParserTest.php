<?php

namespace RealDigital\GuestBook\Tests\unit\TemplateEngine;

use RealDigital\GuestBook\TemplateEngine\MarkerParser;
use PHPUnit\Framework\TestCase;

class MarkerParserTest extends TestCase
{

    public function testRender()
    {
        $markerParser = new MarkerParser();
        $vars = [
          'test' => 'test',
          'marker' => 'marker',
        ];

        $content = 'this is a ###MARKER### ###TEST###';

        $this->assertSame('this is a marker test', $markerParser->render($vars, $content));
    }

    public function testGetVarFromContent()
    {
        $markerParser = new MarkerParser();

        $content = '###TEST### ###TEST### ###MARKER###';
        $matches = $markerParser->getVarFromContent($content);

        $this->assertSame(['test', 'marker'], $matches);
    }

    public function test_ReturnEmptyArray_When_TryingToGetVarsWithoutAMatch()
    {
        $markerParser = new MarkerParser();

        $content = 'jasdhjkashdlkjhkasbdhksasfg';
        $matches = $markerParser->getVarFromContent($content);

        $this->assertSame([], $matches);
    }
}
