<?php

namespace RealDigital\GuestBook\Tests\unit\TemplateEngine;

use RealDigital\GuestBook\Exception\TemplateMissingException;
use RealDigital\GuestBook\TemplateEngine\HtmlTemplate;
use PHPUnit\Framework\TestCase;

class HtmlTemplateTest extends TestCase
{
    protected function setUp()
    {
        $_SERVER['DOCUMENT_ROOT'] = __DIR__;
        parent::setUp();
    }

    public function test_SetTemplate_When_SettingTemplateThatExists()
    {
        $htmlTemplate = new HtmlTemplate('/html/');
        $htmlTemplate->setTemplate('TemplateWithMarker');

        $this->assertSame('this is a ###MARKER###', $htmlTemplate->getContent());
    }

    public function test_SetTemplateFail_When_SettingTemplateThatDoesNotExist()
    {
        $htmlTemplate = new HtmlTemplate('/html/');
        try {
            $htmlTemplate->setTemplate('TemplateThatDoesNotExist');
        } catch (TemplateMissingException $exception) {
            $this->assertInstanceOf(TemplateMissingException::class, $exception);
        }
    }
}