<?php

namespace RealDigital\GuestBook\Tests\unit\TemplateEngine;

use PHPUnit\Framework\TestCase;
use RealDigital\GuestBook\TemplateEngine\MarkerParser;
use RealDigital\GuestBook\TemplateEngine\TemplateInterface;
use RealDigital\GuestBook\TemplateEngine\View;

class ViewTest extends TestCase
{
    public function test_VariableIsSet_When_UsingAssignSingle()
    {
        $mockTemplate = $this->createMock(TemplateInterface::class);
        $mockMarkerParser = $this->createMock(MarkerParser::class);
        $mockMarkerParser->expects($this->once())
            ->method('render')->with(['test' => 'test'], '');
        $view = new View($mockTemplate, $mockMarkerParser);

        $view->assignSingle('test', 'test');
        $view->render();
    }

    public function test_VariableIsSet_When_UsingAssignSingleTwice()
    {
        $mockTemplate = $this->createMock(TemplateInterface::class);
        $mockMarkerParser = $this->createMock(MarkerParser::class);
        $mockMarkerParser->expects($this->once())
            ->method('render')->with(
                [
                    'test' => 'test',
                    'marker' => 'marker',
                ],
                ''
            );
        $view = new View($mockTemplate, $mockMarkerParser);

        $view->assignSingle('test', 'test');
        $view->assignSingle('marker', 'marker');
        $view->render();
    }

    public function test_VariableIsSet_When_UsingAssignMulti()
    {
        $assignArray = [
            'test' => 'test',
            'marker' => 'marker',
            'bla' => 'bla',
        ];
        $mockTemplate = $this->createMock(TemplateInterface::class);
        $mockMarkerParser = $this->createMock(MarkerParser::class);
        $mockMarkerParser->expects($this->once())->method('render')->with($assignArray, '');
        $view = new View($mockTemplate, $mockMarkerParser);

        $view->assignMulti($assignArray);
        $view->render();
    }

    public function test_ContentGivenToTemplate_When_SetTemplateUsed()
    {
        $template = 'This is a test Template';
        $mockTemplate = $this->createMock(TemplateInterface::class);
        $mockMarkerParser = $this->createMock(MarkerParser::class);
        $mockTemplate->expects($this->once())->method('setTemplate')->with($template);
        $view = new View($mockTemplate, $mockMarkerParser);

        $view->setTemplate($template);
    }

    public function test_UnusedVariablesGetDeleted_When_VariablesAreNotSet()
    {
        $mockTemplate = $this->createMock(TemplateInterface::class);
        $mockMarkerParser = $this->createMock(MarkerParser::class);
        $mockMarkerParser->method('getVarFromContent')->willReturn(['test', 'marker', 'removed']);
        $mockMarkerParser->expects($this->once())
            ->method('render')->with(
                [
                    'test' => 'test',
                    'marker' => 'marker',
                    'removed' => '']
            );
        $view = new View($mockTemplate, $mockMarkerParser);
        $view->assignSingle('test', 'test');
        $view->assignSingle('marker', 'marker');
        $view->render();
    }

    public function test_variablesGetNotDeleted_When_ReplaceNotAssignedVariablesIsSetFalse()
    {
        $mockTemplate = $this->createMock(TemplateInterface::class);
        $mockMarkerParser = $this->createMock(MarkerParser::class);
        $mockMarkerParser->method('getVarFromContent')->willReturn(['test', 'marker', 'removed']);
        $mockMarkerParser->expects($this->once())
            ->method('render')->with(
                [
                    'test' => 'test',
                    'marker' => 'marker',
                    ]
            );
        $view = new View($mockTemplate, $mockMarkerParser);
        $view->assignSingle('test', 'test');
        $view->assignSingle('marker', 'marker');
        $view->render(false);
    }
}
