<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Test;

use PHPUnit\Framework\TestCase;
use RealDigital\GuestBook\Message\SuccessMessage;

class SuccessMessageTest extends TestCase
{
    public function test_ReturnSuccessMessageInterface_FromgetType(): void
    {
        $message = 'test';
        $successMessage = new SuccessMessage($message);

        $this->assertSame(SuccessMessage::TYPE_SUCCESS, $successMessage->getType());
    }

    public function test_ReturnedMessageInArrayEqualsSetMessage_WhenMessageIsSet(): void
    {
        $message = 'test';
        $successMessage = new SuccessMessage($message);
        $successMessage = $successMessage->toArray();

        $this->assertSame($message, $successMessage['text']);
    }

    public function test_ReturnCorrectMessageText_WhenCreatingSuccessMessage(): void
    {
        $message = 'test';
        $successMessage = new SuccessMessage($message);

        $this->assertSame('<div class="alert alert-success" role="alert">test</div>', $successMessage->getMessage());
    }
}