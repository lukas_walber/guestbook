<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Test;

use PHPUnit\Framework\TestCase;
use RealDigital\GuestBook\Message\WarningMessage;

class WarningMessageTest extends TestCase
{
    public function test_ReturnWarningMessageInterface_FromgetType(): void
    {
        $message = 'test';
        $warningMessage = new WarningMessage($message);

        $this->assertSame(WarningMessage::TYPE_WARNING, $warningMessage->getType());
    }

    public function test_ReturnedMessageInArrayEqualsSetMessage_WhenMessageIsSet(): void
    {
        $message = 'test';
        $warningMessage = new WarningMessage($message);
        $warningMessage = $warningMessage->toArray();

        $this->assertSame($message, $warningMessage['text']);
    }

    public function test_ReturnCorrectMessageText_WhenCreatingWarningMessage(): void
    {
        $message = 'test';
        $warningMessage = new WarningMessage($message);

        $this->assertSame('<div class="alert alert-warning" role="alert">test</div>', $warningMessage->getMessage());
    }
}