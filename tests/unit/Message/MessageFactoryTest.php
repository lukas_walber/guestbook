<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Test;

use PHPUnit\Framework\TestCase;
use RealDigital\GuestBook\Message\ErrorMessage;
use RealDigital\GuestBook\Message\InfoMessage;
use RealDigital\GuestBook\Message\MessageFactory;
use RealDigital\GuestBook\Message\MessageInterface;
use RealDigital\GuestBook\Message\SuccessMessage;
use RealDigital\GuestBook\Message\WarningMessage;

class MessageFactoryTest extends TestCase
{
    public function test_ShouldReturnErrorMessage_When_ErrorMessageIsGiven(): void
    {
        $messageError = [
            'text' => 'test',
            'type' => MessageInterface::TYPE_ERROR,
        ];

        $this->assertInstanceOf(ErrorMessage::class, MessageFactory::getMessageFromArray($messageError));
    }

    public function test_ShouldReturnInfoMessage_When_InfoMessageIsGiven(): void
    {
        $messageInfo = [
            'text' => 'test',
            'type' => MessageInterface::TYPE_INFO,
        ];

        $this->assertInstanceOf(InfoMessage::class, MessageFactory::getMessageFromArray($messageInfo));
    }

    public function test_ShouldReturnSuccessMessage_When_SuccessMessageIsGiven(): void
    {
        $messageSuccess = [
            'text' => 'test',
            'type' => MessageInterface::TYPE_SUCCESS,
        ];

        $this->assertInstanceOf(SuccessMessage::class, MessageFactory::getMessageFromArray($messageSuccess));
    }

    public function test_ShouldReturnWarningMessage_When_WarningMessageIsGiven(): void
    {
        $messageWarning = [
            'text' => 'test',
            'type' => MessageInterface::TYPE_WARNING,
        ];

        $this->assertInstanceOf(WarningMessage::class, MessageFactory::getMessageFromArray($messageWarning));
    }
    public function test_ShouldReturnNull_When_NonExistingMessageIsGiven(): void
    {
        $this->assertSame(null, MessageFactory::getMessageFromArray([]));
    }
}