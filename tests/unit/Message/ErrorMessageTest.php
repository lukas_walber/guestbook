<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Test;

use PHPUnit\Framework\TestCase;
use RealDigital\GuestBook\Message\ErrorMessage;

class ErrorMessageTest extends TestCase
{
    public function test_MessageTypeIsRight_When_MessageIsSetUp(): void
    {
        $message = 'test';
        $errorMessage = new ErrorMessage($message);

        $this->assertSame(ErrorMessage::TYPE_ERROR, $errorMessage->getType());;
    }

    public function test_MessageIsEqual_When_MessageIsSet(): void
    {
        $message = 'test';
        $errorMessage = new ErrorMessage($message);
        $errorMessage = $errorMessage->toArray();

        $this->assertSame($message, $errorMessage['text']);
    }

    public function test_CorrectMessageTextIsShown_When_CreatingErrorMessage(): void
    {
        $message = 'test';
        $errorMessage = new ErrorMessage($message);

        $this->assertSame('<div class="alert alert-danger" role="alert">test</div>', $errorMessage->getMessage());
    }
}