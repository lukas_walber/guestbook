<?php
declare(strict_types=1);

namespace RealDigital\GuestBook\Test;

use PHPUnit\Framework\TestCase;
use RealDigital\GuestBook\Message\InfoMessage;

class InfoMessageTest extends TestCase
{
    public function test_ReturnInfoMessageInterface_FromgetType(): void
    {
        $message = 'test';
        $infoMessage = new InfoMessage($message);

        $this->assertSame(InfoMessage::TYPE_INFO, $infoMessage->getType());
    }

    public function test_ReturnedMessageInArrayEqualsSetMessage_WhenMessageIsSet(): void
    {
        $message = 'test';
        $infoMessage = new InfoMessage($message);
        $infoMessage = $infoMessage->toArray();

        $this->assertSame($message, $infoMessage['text']);
    }

    public function test_ReturnCorrectMessageText_WhenCreatingInfoMessage(): void
    {
        $message = 'test';
        $infoMessage = new InfoMessage($message);

        $this->assertSame('<div class="alert alert-info" role="alert">test</div>', $infoMessage->getMessage());
    }
}