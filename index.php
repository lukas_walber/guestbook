<?php
declare(strict_types=1);


require_once __DIR__ . '/vendor/autoload.php';

use RealDigital\GuestBook\Bootstrap\Router;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

$containerBuilder = new ContainerBuilder();
$loader = new YamlFileLoader($containerBuilder, new FileLocator(__DIR__ . '/config'));
$loader->load('services.yaml');


$guestbookController = $containerBuilder->get('controller.forum.guestbook_controller');
$loginController = $containerBuilder->get('controller.auth.login_controller');
$logoutController = $containerBuilder->get('controller.auth.logout_controller');
$registrationController = $containerBuilder->get('controller.auth.registration_controller');
$verificationController = $containerBuilder->get('controller.auth.verification_controller');
$profileController = $containerBuilder->get('controller.forum.profile_controller');
$postController = $containerBuilder->get('controller.post.post_controller');
$settingController = $containerBuilder->get('controller.user.setting_controller');

$router = new Router();

$router->add('/', [$guestbookController, 'show']);
$router->add('/login', [$loginController, 'show']);
$router->add('/login/request', [$loginController, 'request']);
$router->add('/logout/request', [$logoutController, 'request']);
$router->add('/registration', [$registrationController, 'show']);
$router->add('/registration/request', [$registrationController, 'request']);
$router->add('/verification/request', [$verificationController, 'request']);
$router->add('/profile', [$profileController, 'show']);
$router->add('/post/request', [$postController, 'request']);
$router->add('/user/settings', [$settingController, 'show']);
$router->add('/user/settings/request', [$settingController, 'request']);

$router->run();